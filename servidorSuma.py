#!/usr/bin/python3
import socket
import selectors
import types

host = "127.0.0.1"
port = 12345



def accept_wrapper(sock):
    conn, addr = sock.accept()
    print('Conexion aceptada: ', addr)
    conn.setblocking(False)
    data    = types.SimpleNamespace(addr=addr, inb=b'', outb=b'')
    events  = selectors.EVENT_READ | selectors.EVENT_WRITE
    data = types.SimpleNamespace(connid=connid,
                                     msg_total=sum(len(m) for m in messages),
                                     recv_total=0,
                                     messages=list(messages),
                                     outb=b'')
    sel.register(conn, events, data=data)

def service_connection(key, mask):
    sock = key.fileobj
    data = key.data
    # sum = 0    
    if mask & selectors.EVENT_READ:
        recv_data = sock.recv(1024)
        if recv_data:
            print('Recibido: ', repr(recv_data), 'desde conexion: ', data.connid)
            data.recv_total += len(recv_data)
            # data.outb  += recv_data
            # cadena = data.outb
            # print(cadena.decode('UTF-8'))
            # for aux in cadena.decode('UTF-8').split():
                # if aux.isdigit():
                    # sum += int(aux)
                    # print(sum)
           
                # print(type(aux))
            # sum+=2
            # sum += [int(aux) for aux in str(data.outb.decode('utf')).split() if aux.isdigit()] 
            # sum+=int( data.outb.decode('utf-8') )
        if not recv_data or data.recv_total == data.msg_total:
            print('Cerrando conexion a: ', data.addr)
            sel.unregister(sock)
            sock.close()
    if mask & selectors.EVENT_WRITE:
        if not data.outb  adn data.messages:
            data.outb = data.messages.pop(0)
        if data.outb:
            print('Enviando', repr(data.outb), 'a: ', data.addr)
            sent = sock.send(data.outb)
            # sumaStr = bytes(str(sum),'utf-8')
            # sent = sock.send(bytes( str(sum), 'UTF-8' ) )
            data.outb = data.outb[sent:]

sel = selectors.DefaultSelector()

lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
lsock.bind((host, port))
lsock.listen()
print('Escuchando en: ', (host, port))
lsock.setblocking(False)
sel.register(lsock, selectors.EVENT_READ, data=None)

while True:
    events = sel.select(timeout=None)
    for key, mask in events:
        if key.data is None:
            accept_wrapper(key.fileobj)
        else:
            service_connection(key, mask)
