#!/usr/bin/python3
import socket
import selectors
import types
import pickle
import time
# import struct

host = "127.0.0.1"
port = 12345
num_conns = 2
# lstNumeros = [bytes(str(5),'utf-8'),bytes(str(3),'utf-8')]
# messages = pickle.dumps(lstNumeros)
# messages = [bytes(str(5),'utf-8'),bytes(str(3),'utf-8')]
messages = b''+str(5)+' '+str(4)
def start_connections(host, port, num_conns):
    server_addr = (host, port)
    for i in range(0, num_conns):
        connid = i + 1
        # time.sleep(2)
        print('Iniciando conexion:', connid, 'a', server_addr)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setblocking(False)
        sock.connect_ex(server_addr)
        events = selectors.EVENT_READ | selectors.EVENT_WRITE
        data = types.SimpleNamespace(connid=connid,
                                     msg_total=sum(len(m) for m in messages),
                                     recv_total=0,
                                     messages=list(messages),
                                     outb='' )
        sel.register(sock, events, data=data)

def service_connection(key, mask):
    sock = key.fileobj
    data = key.data

    # Si esta disponible la lectura
    if mask & selectors.EVENT_READ:
        recv_data = sock.recv(1024)
        # Si hay datos
        if recv_data:
            print('Recibido: ', repr(recv_data), 'desde conexion: ', data.connid)
            data.recv_total += len(recv_data)
        # Si no hay datos OR el ttotal de lo receibido
        if not recv_data or data.recv_total == data.msg_total:
            print('Cerrando conexion', data.connid)
            sel.unregister(sock)
            sock.close()
    # Si el evento WRITE esta disponible
    if mask & selectors.EVENT_WRITE:
        # Si no hay datos en outb AND hay mensajes
        if not data.outb and data.messages:
            data.outb = data.messages.pop(0)
        # Si el outb tiene datos los envia
        if data.outb:
            # data = struct.pack('<3f', data.outb)
            print('Enviando', repr(data.outb), 'a conexion: ', data.connid)
            # sent = sock.send(  pickle.dump(data.outb) )
            sent = sock.send( bytes(str(data.outb) , 'utf-8'))
            data.outb = data.outb[sent:]




sel = selectors.DefaultSelector()
start_connections(host, port, num_conns)



while True:
    if sel.get_map():
        events = sel.select(timeout=None)
        for key, mask in events:
            service_connection(key, mask)
    else:
        break
