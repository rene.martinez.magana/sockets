#!/usr/bin/env python3
import socket
UDP_IP_ADDRESS = '127.0.0.1'
UDP_PORT = 12345
BUFFER_SIZE = 4096

address = (UDP_IP_ADDRESS, UDP_PORT)
socket_cliente = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True:
    mensaje = input('Ingresa tu mensaje > ')
    if mensaje == "exit": 
        	break
    socket_cliente.sendto(mensaje.encode(), address)
    respuesta, addr = socket_cliente.recvfrom(BUFFER_SIZE)
    print("Respuesta del servidor => %s" % respuesta.decode("UTF-8"))

socket_cliente.close()
