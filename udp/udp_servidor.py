 #!/usr/bin/env python3
import socket
import sys

UDP_IP_ADDRESS = "0.0.0.0"
UDP_PORT = 12345
buffer = 4096

socket_server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
socket_server.bind((UDP_IP_ADDRESS, UDP_PORT))

while True:
    print("Wating for client...")
    data, address = socket_server.recvfrom(buffer)
    data = data.strip()
    print("Data Received from address:  ", address)
    print("message: ", data.decode("UTF-8"))

    try:
        response = "Hi %s" % sys.platform
    except Exception as e:
        response = "%s" % sys.exc_info()[0]

    print("Response", response)
    socket_server.sendto(response.encode(), address)   
