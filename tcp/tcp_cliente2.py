#!/usr/bin/env python3
import socket

host = '0.0.0.0'
port = 12345
BUFFER_SIZE = 1024

MESSAGE = 'HELLO WORLD, THIS IS MY FIRST MESSAGE '

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_tcp:

    socket_tcp.connect((host, port))

    socket_tcp.send(MESSAGE.encode('utf-8'))

    data = socket_tcp.recv(BUFFER_SIZE)

    print( data.decode('utf-8'))
