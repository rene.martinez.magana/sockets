#!/usr/bin/env python3
import socket

#The client must have the same server specification
#El cliente tiene la misma especificacion del servidor

host = '127.0.0.1'
port = 12345
BUFFER_SIZE = 1024

# MESSAGE = 'HELLO WORLD , THIS IS MY FIRST MESSAGE'
MENSAJE = 1390

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_tcp:
   #nos conectamos al servidor atravez del puerto
    socket_tcp.connect((host, port))

    #we convert str to bytes  
    #socket_tcp.send(MESSAGE.encode('utf-8'))
    socket_tcp.send(str(MENSAJE).encode('utf-8'))
    #se queda escucahndo la respuesta
    data = socket_tcp.recv(BUFFER_SIZE)
    #se muestra la respuesta
    print( data.decode('utf-8'))



