#!/usr/bin/env python3

import socket
#host = '127.0.0.' LOCAL
host = '0.0.0.0'    #REMOTO
port = 12345
BUFFER_SIZE = 1024

#Permite liberar recursos cuando termine el programa
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as socket_tcp:
    #se aparta una direccion y puerto
    socket_tcp.bind( (host, port) )
    #se lanza por conexiones
    socket_tcp.listen()

    connection, addr = socket_tcp.accept()
    with connection:
        print(f'[*] Established connection {addr}')
        while True:
            data =  connection.recv(BUFFER_SIZE)
            if not data:
                break
            else:
                print('[*] Data received: {}'.format(data.decode('utf-8') ))
            #connection.send(data)
            connection.send(bytes("Message from server", "utf-8"))
            


